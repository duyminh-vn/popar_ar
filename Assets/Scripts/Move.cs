using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour
{
    public float moveAmount;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(0f, 0f, moveAmount);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(0f, 0f, -moveAmount);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(moveAmount, 0f, 0f);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(-moveAmount, 0f, 0f);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Translate(0f, moveAmount, 0f);
        }
        if (Input.GetKey(KeyCode.Q))
        {
            transform.Translate(0f, -moveAmount, 0f);
        }
        if (Input.GetKey(KeyCode.R))
        {
            transform.Rotate(0f, moveAmount, 0f);
        }
        if (Input.GetKey(KeyCode.F))
        {
            transform.Rotate(0f, -moveAmount, 0f);
        }
    }
}