using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMoving : MonoBehaviour
{
    [Header("Player")]
    [Tooltip("ABC")]

    [SerializeField] [Range(0f, 4f)] public float speed = 1.0f;
    [SerializeField] Vector3[] myPos;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        int xInput = Random.Range(-100, 100);
        int yInput = Random.Range(-1, 1);
        int zInput = Random.Range(-1, 1);

        float xxInput = Input.GetAxis("Horizontal") * 1.0f;
        float zzInput = Input.GetAxis("Vertical") * 1.0f;

        Vector3 dir = new Vector3(1, 1, 1);

        transform.position += dir;
    }
}
