using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParitcleTriggerControllerNew1 : MonoBehaviour
{
    //[SerializeField] ParticleSystem psMain;

    [SerializeField] float TextWaitTime;

    [SerializeField] float TextRunTime;

    [SerializeField] float TextWaitFadeOutTime;

    [SerializeField] float TextFadeOutTime;

    [SerializeField] float StartAlpha;

    [SerializeField] float EndAlpha;

    [SerializeField] string ReferenceProperty;

    //[SerializeField] Vector3 StartScale;

    //[SerializeField] Vector3 EndScale;

    [SerializeField] Renderer Text;

    [SerializeField] float ParticleWaitTime;

    [SerializeField] float ParticleFadeOutTime;

    [SerializeField] float ParticleStartAlpha;

    [SerializeField] float ParticleEndAlpha;

    [SerializeField] string ParticleReferenceProperty;

    [SerializeField] ParticleSystem[] ps;

    private void Start()
    {
        //psMain.Play();
        StartCoroutine(TextEffect());
        StartCoroutine(ParticleEffect());
    }

    IEnumerator TextEffect()
    {
        Text.material.SetFloat(ReferenceProperty, StartAlpha);
        yield return new WaitForSeconds(TextWaitTime);
        float time = 0;
        while (time < TextRunTime)
        {
            time += Time.deltaTime;
            Text.material.SetFloat(ReferenceProperty, Mathf.Lerp(StartAlpha, EndAlpha, time / TextRunTime));
            yield return null;
        }
        yield return new WaitForSeconds(TextWaitFadeOutTime);
        time = 0;
        while (time < TextFadeOutTime)
        {
            time += Time.unscaledDeltaTime;
            Text.material.SetFloat(ReferenceProperty, Mathf.Lerp(EndAlpha, StartAlpha, time / TextFadeOutTime));
            yield return null;
        }
    }

    IEnumerator ParticleEffect()
    {
        yield return new WaitForSeconds(ParticleWaitTime);
        float time = 0;
        List<float> particleMinSize = new List<float>();
        List<float> particleMaxSize = new List<float>();
        for (int i = 0; i < ps.Length; i++)
        {
            var main = ps[i].main;
            particleMinSize.Add(main.startSize.constantMin);
            particleMaxSize.Add(main.startSize.constantMax);
        }
        while (time < ParticleFadeOutTime)
        {
            float preTime = time;
            time += Time.unscaledDeltaTime;
            for (int i = 0; i < ps.Length; i++)
            {
                var main = ps[i].GetComponent<ParticleSystemRenderer>();
                main.material.SetFloat(ParticleReferenceProperty, Mathf.Lerp(ParticleStartAlpha, ParticleEndAlpha, time / TextRunTime));
                //ParticleSystem.Particle[] particles = new ParticleSystem.Particle[ps[i].particleCount];
                //ps[i].GetParticles(particles);
                //if (particles.Length < 1)
                //    continue;
                //ParticleSystem.MinMaxCurve minMaxCurve = new ParticleSystem.MinMaxCurve();
                //var main = ps[i].main;
                //minMaxCurve.constantMin = particleMinSize[i] - particleMinSize[i] * (time / Time.deltaTime);
                //minMaxCurve.constantMax = particleMaxSize[i] - particleMaxSize[i] * (time / Time.deltaTime);
                //main.startSize = minMaxCurve;
                //for (int j = 0; j < particles.Length; j++)
                //{
                //    float startSize = particles[j].startSize;
                //    particles[j].startSize = startSize - startSize * (Time.unscaledDeltaTime / (ParticleFadeOutTime - preTime));
                //}
                //ps[i].SetParticles(particles);
            }



            yield return null;
        }
    }
}
