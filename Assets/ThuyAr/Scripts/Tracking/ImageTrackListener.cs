using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace ThuyAR
{
    public class ImageTrackListener : MonoBehaviour
    {
        [SerializeField] ARTrackedImageManager m_TrackedImageManager;

        private void Update()
        {
            //string result = aRTrackedImageManager.trackedImagesChanged;
            Debug.Log(
               $"There are {m_TrackedImageManager.trackables.count} images being tracked.");

            //foreach (var trackedImage in m_TrackedImageManager.trackables)
            //{
            //    Debug.Log($"Image: {trackedImage.referenceImage.name} is at " +
            //              $"{trackedImage.transform.position}");
            //}

            ListAllImages();
        }

        //ARTrackedImage GetImageAt(TrackableId trackableId)
        //{
        //    return m_TrackedImageManager.trackables[trackableId];
        //}



        void ListAllImages()
        {
            Debug.Log(
                $"There are {m_TrackedImageManager.trackables.count} images being tracked.");

            foreach (var trackedImage in m_TrackedImageManager.trackables)
            {
                Debug.Log($"Image: {trackedImage.referenceImage.name} is at " +
                         $"{trackedImage.trackingState}");

                if(trackedImage.trackingState == UnityEngine.XR.ARSubsystems.TrackingState.Tracking)
                {
                    Debug.Log($"Image: {trackedImage.referenceImage.name} is at " +
                        $"{trackedImage.transform.position}");
                }

              
            }
        }
    }
}
