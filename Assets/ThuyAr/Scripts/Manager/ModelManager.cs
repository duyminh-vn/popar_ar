using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ThuyAR
{
    public class ModelManager : MonoBehaviour
    {
        public static ModelManager Instance;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        [SerializeField] Transform Parent;
        [SerializeField] GameObject objPreshow;
        [SerializeField] GameObject objShow5Mins;
        [SerializeField] GameObject objShow10Fires;
        [SerializeField] GameObject objCrystals;
        [SerializeField] GameObject objBanner;
        [SerializeField] GameObject objShow10Main;

        private GameObject spawnedObject;
        private GameObject spawnedBanner;

        public void DestroyModel()
        {
            if (spawnedObject != null)
            {
                Destroy(spawnedObject);
            }

            if (spawnedBanner != null)
            {
                Destroy(spawnedBanner);
            }
        }

        public void ShowObjPreShow()
        {
            DestroyModel();
            spawnedObject = Instantiate(objPreshow, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedObject.transform.position = TrackingManager.Instance.transDetected.position;
                //spawnedObject.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public void ShowObjShow5Mins()
        {
            DestroyModel();
            spawnedObject = Instantiate(objShow5Mins, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedObject.transform.position = TrackingManager.Instance.transDetected.position;
                //spawnedObject.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public void ShowObjShow10Fires()
        {
            DestroyModel();
            spawnedObject = Instantiate(objShow10Fires, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedObject.transform.position = TrackingManager.Instance.transDetected.position;
                //spawnedObject.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public void ShowObjCrystals()
        {
            DestroyModel();
            spawnedObject = Instantiate(objCrystals, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedObject.transform.position = TrackingManager.Instance.transDetected.position;
               // spawnedObject.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public void ShowObjBanner()
        {
            DestroyModel();

            spawnedBanner = Instantiate(objBanner, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedBanner.transform.position = TrackingManager.Instance.transDetected.position;
                //spawnedBanner.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public void ShowObjShow10Main()
        {
            DestroyModel();

            spawnedObject = Instantiate(objShow10Main, Vector3.zero, Quaternion.identity, Parent);
            if (TrackingManager.Instance.transDetected != null)
            {
                spawnedObject.transform.position = TrackingManager.Instance.transDetected.position;
                //spawnedObject.transform.rotation = TrackingManager.Instance.transDetected.rotation;
            }
        }

        public bool IsRemovedBanner()
        {
            if (spawnedBanner == null) return true;
            return false;
        }
    }
}