using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour
{
    public static TimeManager Instance;
    public bool isTest;
    public int hourShowGroupB = 23;
    public int hourShowGroupC = 0;
    [SerializeField] private GameObject areaCheat;
    [SerializeField] private Toggle toggle;
    [SerializeField] private TMP_InputField inputHourB;
    [SerializeField] private TMP_InputField inputHourC;

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }

    private void Start()
    {
        if (isTest)
        {
            areaCheat.SetActive(true);
        }
        else
        {
            areaCheat.SetActive(false);
        }
    }

    public void OnToggle()
    {
        if (toggle.isOn)
        {
            inputHourB.gameObject.SetActive(true);
            inputHourC.gameObject.SetActive(true);
        }
        else
        {
            inputHourB.gameObject.SetActive(false);
            inputHourC.gameObject.SetActive(false);
        }
    }

    public void SetHourShowGroupB()
    {
        try
        {
            int h = int.Parse(inputHourB.text);
            if (h >= 0 && h <= 23)
                hourShowGroupB = h;
            Debug.LogError("hourShowGroupB = " + hourShowGroupB);
        }
        catch (Exception e)
        {
            Debug.LogError("Nhap so int");
        }
    }

    public void SetHourShowGroupC()
    {
        try
        {
            int h = int.Parse(inputHourC.text);
            if (h >= 0 && h <= 23)
                hourShowGroupC = h;
            Debug.LogError("hourShowGroupC = " + hourShowGroupC);
        }
        catch (Exception e)
        {
            Debug.LogError("Nhap so int");
        }
    }

    public bool IsTimeShowGroupA(int from, int to)
    {
        if (DateTime.Now.Minute % 10 >= from && DateTime.Now.Minute % 10 < to)
            return true;
        return false;
    }

    public bool IsTimeShowGroupB(int from, int to)
    {
        if (DateTime.Now.Hour == hourShowGroupB)
            if (DateTime.Now.Minute >= from && DateTime.Now.Minute < to)
                return true;
        return false;
    }

    public bool IsTimeShowGroupC(int from, int to)
    {
        if (DateTime.Now.Hour == hourShowGroupC)
            if (DateTime.Now.Minute >= from && DateTime.Now.Minute < to)
                return true;
        return false;
    }

    public bool IsPassMinute(int to)
    {
        if (to == 60)
        {
            if (DateTime.Now.Minute < 59 || (DateTime.Now.Minute == 59 && DateTime.Now.Second < 58))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        else
        {
            if (DateTime.Now.Minute < to)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }

    public bool IsPassMinuteA(int to)
    {
        if (DateTime.Now.Minute % 10 < to)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}