using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Banner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("Remove", 60f);
    }

    private void Remove() {
        DestroyImmediate(gameObject);
    }
}
