using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace ThuyAR
{
    public class MessageManager : MonoBehaviour
    {
        public static MessageManager Instance;

        private void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(this);
            }
            else
            {
                Instance = this;
            }
        }

        [SerializeField] private CanvasGroup PopupSafe;
        [SerializeField] private CanvasGroup PopupPhoto;
        [SerializeField] private CanvasGroup popupCalibMess;
        [SerializeField] private CanvasGroup popupCalibSuccess;
        [SerializeField] private CanvasGroup popupCalibFail;

        public void ShowPopupSafe()
        {
            PopupSafe.DOFade(1, 0.5f);
        }

        public void HidePopupSafe()
        {
            PopupSafe.DOFade(0, 0.5f);
        }

        public void ShowPopupPhoto()
        {
            PopupPhoto.DOFade(1, 0.5f);
        }

        public void HidePopupPhoto()
        {
            PopupPhoto.DOFade(0, 0.5f);
        }

        public void ShowPopupCalibMess()
        {
            popupCalibMess.DOFade(1, 0.5f);
        }

        public void HidePopupCalibMess()
        {
            popupCalibMess.DOFade(0, 0.5f);
        }

        public void ShowPopupCalibSuccess()
        {
            popupCalibSuccess.DOFade(1, 0.5f);
        }

        public void ShowPopupCalibSuccessParallel()
        {
            StopCoroutine(IshowPopupCalibSuccessParallel());
            StartCoroutine(IshowPopupCalibSuccessParallel());
        }

        private IEnumerator IshowPopupCalibSuccessParallel()
        {
            HidePopupCalibMess();
            yield return new WaitForSeconds(1f);
            popupCalibSuccess.DOFade(1, 0.5f);
            yield return new WaitForSeconds(5f);
            popupCalibSuccess.DOFade(0, 0.5f);
        }

        public void HidePopupCalibSuccess()
        {
            popupCalibSuccess.DOFade(0, 0.5f);
        }

        public void ShowPopupCalibFail()
        {
            popupCalibFail.DOFade(1, 0.5f);
        }

        public void HidePopupCalibFail()
        {
            popupCalibFail.DOFade(0, 0.5f);
        }
    }
}