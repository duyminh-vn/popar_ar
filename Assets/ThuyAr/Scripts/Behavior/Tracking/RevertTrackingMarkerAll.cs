using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class RevertTrackingMarkerAll : Conditional
{
    public List<string> nameMarkers;

    public override TaskStatus OnUpdate()
    {
        foreach (var name in nameMarkers)
        {
            if (TrackingManager.Instance.DetectMarker(name))
            {
                return TaskStatus.Failure;
            }
        }

        return TaskStatus.Running;
    }
}