using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ActiveLoopManager : Action
{
    public override TaskStatus OnUpdate()
    {
        TrackingManager.Instance.EnableLoopManager();
        return TaskStatus.Success;
    }
}