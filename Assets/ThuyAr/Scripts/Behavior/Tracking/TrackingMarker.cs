using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class TrackingMarker : Conditional
{
    public string nameMarker;

    public override TaskStatus OnUpdate()
    {
        if (TrackingManager.Instance.DetectMarker(nameMarker))
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}