using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class Show10Mins : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjShow10Fires();
        return TaskStatus.Success;
    }
}