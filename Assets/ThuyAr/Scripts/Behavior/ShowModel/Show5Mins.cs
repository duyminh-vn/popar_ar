using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class Show5Mins : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjShow5Mins();
        return TaskStatus.Success;
    }
}