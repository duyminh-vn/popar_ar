using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class Show1Mins : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjPreShow();
        return TaskStatus.Success;
    }
}