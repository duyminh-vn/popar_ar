using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class DestroyModel : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.DestroyModel();
        return TaskStatus.Success;
    }
}