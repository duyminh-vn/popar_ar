using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowCrystal : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjCrystals();
        return TaskStatus.Success;
    }
}