using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowBanner : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjBanner();
        return TaskStatus.Success;
    }
}