using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class RemovedBanner : Conditional
{
    public override TaskStatus OnUpdate()
    {
        if (ModelManager.Instance.IsRemovedBanner())
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}