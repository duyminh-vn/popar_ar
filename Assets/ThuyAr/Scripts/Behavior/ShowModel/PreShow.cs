using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class PreShow : Action
{
    public override TaskStatus OnUpdate()
    {
        ModelManager.Instance.ShowObjPreShow();
        return TaskStatus.Success;
    }
}