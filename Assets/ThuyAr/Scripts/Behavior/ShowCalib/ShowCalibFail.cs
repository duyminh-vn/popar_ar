using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowCalibFail : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupCalibFail();
        return TaskStatus.Success;
    }
}