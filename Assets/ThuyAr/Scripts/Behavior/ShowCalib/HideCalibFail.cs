using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class HideCalibFail : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.HidePopupCalibFail();
        return TaskStatus.Success;
    }
}