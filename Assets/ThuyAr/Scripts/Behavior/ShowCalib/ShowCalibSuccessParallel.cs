using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowCalibSuccessParallel : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupCalibSuccessParallel();
        return TaskStatus.Success;
    }
}