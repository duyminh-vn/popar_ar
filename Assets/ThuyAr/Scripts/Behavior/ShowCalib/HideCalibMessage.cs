using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class HideCalibMessage : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.HidePopupCalibMess();
        return TaskStatus.Success;
    }
}