using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowCalibSuccess : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupCalibSuccess();
        return TaskStatus.Success;
    }
}