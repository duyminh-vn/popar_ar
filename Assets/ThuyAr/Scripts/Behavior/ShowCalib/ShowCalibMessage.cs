using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowCalibMessage : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupCalibMess();
        return TaskStatus.Success;
    }
}