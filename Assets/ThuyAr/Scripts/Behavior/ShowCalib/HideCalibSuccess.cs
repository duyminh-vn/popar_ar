using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class HideCalibSuccess : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.HidePopupCalibSuccess();
        return TaskStatus.Success;
    }
}