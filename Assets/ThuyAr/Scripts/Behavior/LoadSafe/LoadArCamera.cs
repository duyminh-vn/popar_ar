using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class LoadArCamera : Conditional
{
    public override TaskStatus OnUpdate()
    {
        TrackingManager.Instance.LoadArCamera();
        return TaskStatus.Success;
    }
}