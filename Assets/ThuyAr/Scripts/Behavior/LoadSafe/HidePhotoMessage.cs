using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class HidePhotoMessage : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.HidePopupPhoto();
        return TaskStatus.Success;
    }
}