using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowPhotoMessage : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupPhoto();
        return TaskStatus.Success;
    }
}