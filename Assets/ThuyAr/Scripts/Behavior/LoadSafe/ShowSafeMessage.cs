using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BehaviorDesigner.Runtime.Tasks;
using ThuyAR;

public class ShowSafeMessage : Action
{
    public override TaskStatus OnUpdate()
    {
        MessageManager.Instance.ShowPopupSafe();
        return TaskStatus.Success;
    }
}