using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class TimeGroupB : Conditional
{
    public int from;
    public int to;

    public override TaskStatus OnUpdate()
    {
        if (TimeManager.Instance.IsTimeShowGroupB(from, to))
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}