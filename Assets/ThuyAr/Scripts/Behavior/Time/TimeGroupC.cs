using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class TimeGroupC : Conditional
{
    public int from;
    public int to;

    public override TaskStatus OnUpdate()
    {
        if (TimeManager.Instance.IsTimeShowGroupC(from, to))
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Failure;
        }
    }
}