using System.Collections;
using System.Collections.Generic;
using BehaviorDesigner.Runtime.Tasks;
using UnityEngine;

public class WaitEndShow : Action
{
    public int to;

    public override TaskStatus OnUpdate()
    {
        if (TimeManager.Instance.IsPassMinute(to))
        {
            return TaskStatus.Success;
        }
        else
        {
            return TaskStatus.Running;
        }
    }
}